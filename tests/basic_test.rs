
extern crate stonefruit;

use std::collections::{BTreeMap, BTreeSet};
use stonefruit::types::{Storable, FieldLabel, TypeLabel, KindingEnvironment, Kind};
use stonefruit::constraints::Constraint;
use stonefruit::ast::{AstBone};

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord, Hash)]
struct Ohori {
    labels: BTreeSet<FieldLabel>,
    exact: bool,
}
impl stonefruit::types::Storable for Ohori {}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord, Hash)]
enum BasicType {
    Integer,
    Float,
}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord, Hash)]
struct Position(i32);

impl Storable for BasicType {}
impl Storable for Position {}

impl Constraint for Ohori {

    fn entails(&self, rhs: &Ohori) -> bool {
        if rhs.exact { return self == rhs }
        (&rhs.labels).is_subset(&self.labels)
    }

    fn is_valid(&self) -> bool {
        true
    }

    fn and(&self, other: &Self) -> Self {
        Ohori {
            labels: self.labels.union(&other.labels)
                               .map(|x|x.to_string())
                               .collect::<BTreeSet<_>>(),
            exact: self.exact | other.exact,
        }
    }

    fn observes(&self, _: FieldLabel) -> bool {
        true
    }

}

mod reconstruction {
    use super::*;

    use stonefruit::ast::Ast;
    use stonefruit::ast::AstBone::*;

    use stonefruit::types::{Polytype, Monotype};

    #[test]
    fn test_basic_constant() {
        let float_constant: AstBone<Ohori, BasicType, Position> = Const(Polytype::mono(Monotype::Base(BasicType::Float)));
        let phi = stonefruit::solve(Ast::new(Position(1), float_constant)).unwrap();
    }

    #[test]
    fn test_constraint_constant() {
        let a1 = TypeLabel::new(Position(1));
        let a0 = TypeLabel::new(Position(2));
        let mut kenv = KindingEnvironment::new();
        let mut labels = BTreeSet::new();
        let mut relations = BTreeMap::new();
        labels.insert("color".to_string());
        relations.insert("color".to_string(), Monotype::Generic(a1.clone()));
        kenv.insert(a1.clone(), Kind { constraint: Ohori {labels, exact: true}, relations } );
        let record_constant: AstBone<Ohori, BasicType, Position> = Const(Polytype {
            kinding_environment: kenv,
            monotype: Monotype::Arrow(a1, a0)
        });
        let phi = stonefruit::solve(Ast::new(Position(1), record_constant)).unwrap();
    }
}
