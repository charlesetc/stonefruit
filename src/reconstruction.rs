
use ast::{Ast, AstBone};
use phi::{Phi, PhiResult};
use letenvironment::LetEnvironment;
use types::{Polytype, Monotype, Storable};
use constraints::Constraint;
use quantifiable::{Quantifiable, Substitutable, duplicate};
use std::collections::{BTreeSet, BTreeMap};

/// Just to add a method to ast, while keeping it in a separate file.
pub trait Reconstruct<C, U, P> where C: Constraint, U: Storable, P: Storable {
    fn reconstruct(&self, &mut LetEnvironment<C, U, P>, &mut Phi<C, U, P>) -> PhiResult<(), P>;
}

/// What happens here is we take an ast and generate a bunch of calls
/// to the phi, which ultimately will do unification.
impl<C, U, P> Reconstruct<C, U, P> for Ast<C, U, P>
where C: Constraint, U: Storable, P: Storable {
    fn reconstruct(&self, env: &mut LetEnvironment<C, U, P>, phi: &mut Phi<C, U, P>) -> PhiResult<(), P> {
        use self::AstBone::*;
        let &Ast { ref typelabel, ref bone, ref position } = self;

        // Each ast's starting type is added to phi, to point to itself.
        phi.insert_for_first_time(typelabel.clone());

        Ok(match bone {
            &Fun(ref var, ref body) => {
                let name = var.variable_name().expect("Arguments to functions must be variables.");
                phi.insert_for_first_time(var.typelabel.clone());
                phi.insert_for_first_time(body.typelabel.clone());
                let arrow = phi.new_type(
                    Monotype::Arrow(var.typelabel.clone(), body.typelabel.clone()),
                    position.clone());
                phi.type_equals(typelabel.clone(), arrow)?;
                let state = env.state();
                env.insert(name, Polytype::mono(Monotype::Generic(var.typelabel.clone())));
                body.reconstruct(env, phi)?;
                env.set_state(state);
            }
            &Apply(ref f, ref arg) => {
                let arrow = phi.new_type(
                    Monotype::Arrow(arg.typelabel.clone(), typelabel.clone()),
                    position.clone());
                phi.type_equals(f.typelabel.clone(), arrow)?;
                arg.reconstruct(env, phi)?;
                f.reconstruct(env, phi)?;
            }
            &Const(ref polytype) => {
                let polytype = duplicate(polytype, phi);
                let new_label = phi.new_type(polytype.monotype, position.clone());
                phi.type_equals(typelabel.clone(), new_label)?;

                for (ref typelabel, ref kind) in polytype.kinding_environment {
                    phi.kind_equals(typelabel.clone(), kind.clone())?;
                }
            }
            &Var(ref name) => {
                let polytype = env.lookup(&name)
                                  .expect(&format!("undefined variable {} at {:?}", name, position));

                let polytype = duplicate(polytype, phi);

                let new_label = phi.new_type(polytype.monotype, position.clone());
                try!(phi.type_equals(typelabel.clone(), new_label));

                for (typelabel, kind) in polytype.kinding_environment.iter() {
                    phi.kind_equals(typelabel.clone(), kind.clone())?;
                }
            }
            &Let(ref name, ref expr, ref body) => {

                // 1. make a map from the solved subphi.
                // 2. apply this map to the let environment
                // 3. figure out what B is
                // 4. do proper constraints.

                let mut subphi = Phi::new();
                expr.reconstruct(env, &mut subphi)?;
                let theta = subphi.construct_theta();
                let alpha: Monotype<U, P> = Monotype::Generic(expr.typelabel.clone());
                phi.merge_phi(&subphi);

                let k = &subphi.kinding_environment;
                let b_first_part = alpha.substitute(&theta).free_variables(phi, k);
                let b_second_part = env.substitute(&theta).free_variables(phi, k);
                let b: BTreeSet<_> = b_first_part.difference(&b_second_part).cloned().collect();

                let mut k_sub_b = BTreeMap::new();
                for label in &b {
                    k_sub_b.insert(label.clone(), k[label].clone());
                }


                // This part is the phi_sub_not_b on the left in the paper.
                let mut k_sub_not_b = k.clone();
                for label in &b {
                    k_sub_not_b.remove(label);
                }

                let type_of_expr = Polytype {
                    kinding_environment: k_sub_b,
                    monotype: alpha.substitute(&theta),
                };

                let state = env.state();
                env.insert(name.clone(), type_of_expr);
                body.reconstruct(env, phi)?;
                env.set_state(state); // expr is no longer applicable.
            }
        })
    }
}
