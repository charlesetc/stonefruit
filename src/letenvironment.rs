
use constraints::Constraint;
use types::{Storable, Polytype, VariableLabel};

/// A let environment is similar to the kinding environment,
/// in that it's passed around everywhere and keeps track of types.
/// It's Γ in the paper (capital gamma). It's easier to understand
/// than kinding environments: It's a mapping of variable names to
/// their *polytypes*.
pub struct LetEnvironment<C: Constraint, U: Storable, P: Storable> {
    /// This is a vector because it's representing a stack.
    /// When the scope of the variable is done, we reset to
    /// the state before the scope.
    pub bindings: Vec<(VariableLabel, Polytype<C, U, P>)>,
}

impl<C, U, P> LetEnvironment<C, U, P>
where C: Constraint, U: Storable, P: Storable {

    pub fn new() -> LetEnvironment<C, U, P> {
        LetEnvironment {
            bindings: Vec::new(),
        }
    }

    /// Keep track of a new variable, and it's type.
    pub fn insert(&mut self, label: VariableLabel, _type: Polytype<C, U, P>) {
        self.bindings.push((label, _type));
    }

    /// Lookup lets you get a type from a variable name.
    /// It looks up in reverse order so that if there are
    /// two bindings with the same variable name, we'll get
    /// the most recently bound.
    pub fn lookup(&self, label: &VariableLabel) -> Option<&Polytype<C, U, P>> {
        for binding in self.bindings.iter().rev() {
            let &(ref binding_label, ref binding_type) = binding;
            if binding_label == label {
                return Some(binding_type)
            }
        }
        return None
    }

    /// The `state` and `set_state` functions are used to reset the
    /// stack when a function returns or a let binding is done.
    /// In this particular case, we just keep track of the length
    /// of the stack and reset to that, but a naive solution would
    /// be to copy the bindings and just switch out the old bindins
    /// at the end of the function.
    pub fn state(&self) -> usize {
        self.bindings.len()
    }

    pub fn set_state(&mut self, size: usize) {
        while self.bindings.len() > size {
            self.bindings.pop();
        }
    }
}