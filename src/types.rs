
use constraints::Constraint;

use atomic;
use std::hash::Hash;
use std::collections::BTreeMap;
use std::fmt::Debug;

/// Useful as a shorthand for writing all these traits.
pub trait Storable : Eq + Clone + Ord + Debug + Hash {}

/// A monotype is a type that just has base types,
/// arrow types, or generic types. Notably, it does
/// not have a kinding environment or constraints.
///
/// It's parametrized on `U`, which is where the user
/// of stonefruit will fill their Base types --
/// think integers, strings, etc.
///
/// Additionally, you'll notice that generics and arrows
/// have only type labels and not other monotypes. This is
/// because their extra data is stored in a phi somewhere.
/// If you ever want to find the full type of a monotype,
/// make sure to have a phi handy.
#[derive(Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Monotype<U: Storable, P: Storable> {
    Base(U),
    Generic(TypeLabel<P>),
    Arrow(TypeLabel<P>, TypeLabel<P>),
}

/// Needs to be explicitly implemented.
impl<U: Storable, P: Storable> Storable for Monotype<U, P> {}

impl<U: Storable, P: Storable> Monotype<U, P> {
    /// Used to make a new generic monotype
    pub fn generic(p: P) -> Monotype<U, P> {
        Monotype::Generic(TypeLabel::new(p))
    }

    pub fn base_type(self) -> U {
        match self {
            Monotype::Base(u) => u,
            _ => panic!("called base type on {:?}", self.clone()),
        }
    }
}

/// A kinding environment is found on Polytypes and on the phi.
pub type KindingEnvironment<C, U, P> = BTreeMap<TypeLabel<P>, Kind<C, U, P>>;

/// A polytype is the more complex type:
/// it's a monotype, with
/// an accompanying kinding environment.
///
/// A key insight here is that, while polytypes can
/// be functions (since monotypes can be functions),
/// neither the argument nor the return value can be a
/// polytype.
#[derive(Debug, PartialEq, Eq, Hash, Clone, PartialOrd, Ord)]
pub struct Polytype<C: Constraint, U: Storable, P: Storable> {
    pub kinding_environment: KindingEnvironment<C, U, P>,
    pub monotype: Monotype<U, P>,
}

impl<C: Constraint, U: Storable, P: Storable> Polytype<C, U, P> {
    /// Make a polytype from a monotype
    pub fn mono(m: Monotype<U, P>) -> Polytype<C, U, P> {
        Polytype {
            kinding_environment: KindingEnvironment::new(),
            monotype: m,
            
        }
    }
}

/// A kind has two parts: a constraint and a set of relations.
/// Exactly what the constraint is left up to the user of the library,
/// but it's something to do with FieldLabels - i.e. some particular field
/// name for an object, or a field name. Check out constraints.rs for more.
/// The set of relations is what maps each of these feild names to a monotype.
#[derive(Debug, PartialEq, Eq, Hash, Clone, PartialOrd, Ord)]
pub struct Kind<C, U: Storable, P:Storable> {
    pub constraint: C,
    pub relations: BTreeMap<FieldLabel, Monotype<U, P>>,
}

impl<C: Constraint, U: Storable, P: Storable> Kind<C, U, P> {
}

/// A variable label is the actual name of
/// a variable. Could be interned in the future.
pub type VariableLabel = String;

/// A field label is the name of a field in a structure
/// Also could be interned in the future.
pub type FieldLabel = String;

/// A type label is the identifier for a Generic type.
/// There's also a position, so we can give nice error
/// messages.
///
/// It's used as the key in the phi graph.
#[derive(Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct TypeLabel<P: Storable> {
    pub id: usize,
    pub positions: Vec<P>,
}

impl<P: Storable> TypeLabel<P> {
    pub fn new(position: P) -> TypeLabel<P> {
        TypeLabel {
            id: atomic::new(),
            positions: vec![position],
        }
    }

    pub fn new_vec(positions: Vec<P>) -> TypeLabel<P> {
        TypeLabel {
            id: atomic::new(),
            positions: positions,
        }
    }

    pub fn new_from_two(a: &TypeLabel<P>, b: &TypeLabel<P>) -> TypeLabel<P> {
        let mut ps = vec![];
        ps.extend(a.positions.iter().cloned());
        ps.extend(b.positions.iter().cloned());
        TypeLabel {
            id: atomic::new(),
            positions: ps,
        }
    }
}
