
use std::marker::Sized;
use types;

/// The Constraint trait is never implemented within the library
/// and has to be implemented by the user of the library for anything
/// more than an HM system.
///
/// These functions correspond to the Constraint domain definitions
/// in the paper, and so with this concept of constraint, changing
/// its implementation results in systems of differing inference
/// abilities.
pub trait Constraint : Sized + types::Storable {

    /// This is the entailement relation.
    /// There's a bottom element in the paper, although
    /// no bottom is needed here. Instead, constraints are
    /// said to entail bottom when they are not valid.
    fn entails(&self, &Self) -> bool;

    /// This is the observation relation.
    /// It's used in the paper to remove type equivalences
    /// when there is no need for them to be observed. It's
    /// highly dependent on the implementation of `Constraint`,
    /// but it allows for things like masking, when a masked
    /// feild need not be observed (i.e. be constrained
    /// to be the same type as other masked fields.)
    fn observes(&self, types::FieldLabel) -> bool;

    fn and(&self, &Self) -> Self;

    fn is_valid(&self) -> bool;

}