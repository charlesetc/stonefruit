
use std::collections::BTreeMap;
use constraints::Constraint;
use types::{TypeLabel, Kind, Storable, Monotype, KindingEnvironment};
use quantifiable::Quantifiable;

/// Phi is the unification problem in the paper.
///
/// It consists of a kinding environment and a
/// graph of nodes. The insight is that monotypes
/// have typelabels, which are themselves references
/// to nodes in phi. In this way, it becomes
/// a context that's passed around a lot during
/// type reconstruction: any type that needs to be
/// read and has the potential of being an arrow has
/// to have access to phi.
#[derive(Debug)]
pub struct Phi<C: Constraint, U: Storable, P: Storable> {
    pub kinding_environment: KindingEnvironment<C, U, P>,
    hm_graph: BTreeMap<TypeLabel<P>, Monotype<U, P>>,
    constraint_graph: BTreeMap<TypeLabel<P>, Monotype<U, P>>,
}

pub type PhiResult<T, P> = Result<T, PhiError<P>>;

#[derive(Debug)]
pub struct PhiError<P: Storable> {
    positions: Vec<P>,
    description: String,
}

/// As in "what sort of berries?"
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Sort {
    Base,
    Arrow,
    Kinded,
    Unbound,
}

impl<C: Constraint, U: Storable, P: Storable> Phi<C, U, P> {

    pub fn new() -> Phi<C, U, P> {
        Phi {
            kinding_environment: BTreeMap::new(),
            hm_graph: BTreeMap::new(),
            constraint_graph: BTreeMap::new(),
        }
    }

    pub fn kind_equals(&mut self, typelabel: TypeLabel<P>, kind: Kind<C, U, P>) -> Result<(), PhiError<P>>{
        // Bad Constraint rule
        if !kind.constraint.is_valid() {
            return self.error1(&typelabel, "invalid kind");
        }
        self.kinding_environment.insert(typelabel, kind);
        Ok(())
    }

    fn get_kind(&self, typelabel: TypeLabel<P>) -> Option<Kind<C, U, P>> {
        self.kinding_environment.get(&typelabel).cloned()
    }

    pub fn insert_for_first_time(&mut self, t: TypeLabel<P>) {
        if self.constraint_graph.get(&t).is_none() {
            self.constraint_graph.insert(t.clone(), Monotype::Generic(t.clone()));
            self.hm_graph.insert(t.clone(), Monotype::Generic(t));
        }
    }

    pub fn new_type(&mut self, monotype: Monotype<U, P>, position: P) -> TypeLabel<P> {
        let label = TypeLabel::new(position);
        self.constraint_graph.insert(label.clone(), monotype.clone());
        self.hm_graph.insert(label.clone(), monotype);
        label
    }

    // pub fn insert(&mut self, typelabel: TypeLabel<P>, monotype: Monotype<U, P>) {
    //     self.graph.insert(typelabel, monotype);
    // }

    pub fn type_equals(&mut self, a: TypeLabel<P>, b: TypeLabel<P>) -> Result<(), PhiError<P>>{
        use self::Sort::*;
        let (a, b) = self.put_into_predictable_order(a, b);

        // # Redundancy rule
        if self.get(&a) == self.get(&b) {
            // If the types are the same,
            // nothing needs to be done.
            return Ok(());
        }

        // # Incompatible rule
        match (self.sort(&a), self.sort(&b)) {
            (Base, Base) if self.find(&a) != self.find(&b) =>
                return self.error2(&a, &b, "base types were expected to be equal"),
            (Kinded, Kinded) |
            (Arrow, Arrow) |
            (Kinded, Unbound) |
            (Base, Unbound) |
            (Arrow, Unbound) |
            (Unbound, Unbound) => (),
            _ => return self.error2(&a, &b, "these types are expected to be the same sort"),
        }

        // # Cyclic rule
        // this covers part of (Kinded, Unbound)
        // part of (Base, Unbound), part of (Kinded, Kinded),
        // and part of (Arrow, Unbound)
        match (self.get(&a), self.get(&b)) {
            (atype, &Monotype::Generic(ref label)) => {
                if atype.free_variables(self, &BTreeMap::new()).contains(label) {
                    return self.error2(&a, &b, "there is a non-unifiable type here");
                }
            }
            _ => (), // on to next rule!
        }

        // # Function rule
        match (self.get(&a).clone(), self.get(&b).clone()) {
            (Monotype::Arrow(x1, y1),
             Monotype::Arrow(x2, y2)) => {
                 self.type_equals(x1, x2)?;
                 self.type_equals(y1, y2)?;
                 // dealt with this case:
                 return Ok(());
            }
            _ => (), // on to next rule!
        }

        // # Substitution rule
        if self.sort(&b) == Unbound &&
            //  //a is a free variable -- not checked atm.
            // this part should check it tau is a free variable or a base.
            // is unbound the same as free variable?
            (self.sort(&a) == Unbound || self.sort(&a) == Base) {
                self.hm_graph.insert(b, Monotype::Generic(a));
                return Ok(());
        }

        // # Constraint rule
        match (self.sort(&a), self.sort(&b)) {
            (Kinded,
             Kinded) => {
                let newlabel = TypeLabel::new_from_two(&a, &b);
                let akind = self.get_kind(a.clone()).unwrap();
                let bkind = self.get_kind(b.clone()).unwrap();
                let mut newrelations = akind.relations.clone();
                newrelations.extend(bkind.relations);
                let newkind = Kind {
                    constraint: akind.constraint.and(&bkind.constraint),
                    relations: newrelations,
                };
                self.kind_equals(newlabel.clone(), newkind)?;

                self.constraint_graph.insert(a, Monotype::Generic(newlabel.clone()));
                self.constraint_graph.insert(b, Monotype::Generic(newlabel));
                return Ok(());
            }
            _ => (), // on to next rule!
        }

        Ok(())

        // unimplemented!()
    }

    fn error2(&self, a: &TypeLabel<P>, b: &TypeLabel<P>, msg: &str) -> Result<(), PhiError<P>> {
        let atype = self.find(a);
        let btype = self.find(b);
        let asort = self.sort(a);
        let bsort = self.sort(b);
        let description = format!("{}: {:?} {:?} and {:?} {:?}",
                                    msg,
                                    asort,
                                    atype.clone(),
                                    bsort,
                                    btype.clone());
        let mut positions = vec![];
        positions.extend(a.positions.iter().cloned());
        positions.extend(b.positions.iter().cloned());
        let err = PhiError { positions, description };
        return Err(err);
    }
    fn error1(&self, a: &TypeLabel<P>, msg: &str) -> Result<(), PhiError<P>> {
        let atype = self.find(a);
        let asort = self.find(a);
        let description = format!("{}: {:?} {:?}",
                                    msg,
                                    asort,
                                    atype.clone());
        let positions = a.positions.clone();
        let err = PhiError { positions, description };
        return Err(err);
    }

    /// As in union-find.
    /// This searches until it finds an end node
    /// or a generic type that points at nothing (i.e) itself.
    pub fn find<'a>(&'a self, typelabel: &'a TypeLabel<P>) -> &'a TypeLabel<P> {
        use self::Monotype::*;
        // This will throw an error if not there:
        match self.constraint_graph[typelabel] {
            Generic(ref newlabel) if newlabel != typelabel => self.find(&newlabel),
            // generic that points to itself
            _ => typelabel,
        }
    }

    pub fn get(&self, typelabel: &TypeLabel<P>) -> &Monotype<U, P> {
        &self.constraint_graph[self.find(typelabel)]
    }

    fn sort(&self, typelabel: &TypeLabel<P>) -> Sort {
        use self::Monotype::*;
        match self.get(typelabel) {
            &Base(_) => Sort::Base,
            &Arrow(_,_) => Sort::Arrow,
            &Generic(ref l)
            if self.kinding_environment.contains_key(l) => Sort::Kinded,
            &Generic(_) => Sort::Unbound,
        }
    }

    fn put_into_predictable_order(&self, a: TypeLabel<P>, b: TypeLabel<P>)
    -> (TypeLabel<P>, TypeLabel<P>) {
        if self.sort(&a) as i32 > self.sort(&b) as i32 {
            (b, a)
        } else {
            (a, b)
        }
    }

    /// Makes a substitution that can be given to the 'substitute method'
    /// of the substitutable trait.
    pub fn construct_theta(&self) -> BTreeMap<TypeLabel<P>, TypeLabel<P>> {
        let mut theta = BTreeMap::new();
        for (typelabel, _) in &self.constraint_graph {
            theta.insert(typelabel.clone(), self.find(typelabel).clone());
        }
        theta
    }

    /// Absorb all the knownlege of another phi.
    pub fn merge_phi(&mut self, phi: &Phi<C, U, P>) {
        for (label, kind) in &phi.kinding_environment {
            self.kinding_environment.insert(label.clone(), kind.clone());
        }
        for (label, _type) in &phi.constraint_graph {
            self.constraint_graph.insert(label.clone(), _type.clone());
        }
        for (label, _type) in &phi.hm_graph {
            self.hm_graph.insert(label.clone(), _type.clone());
        }
    }
}
