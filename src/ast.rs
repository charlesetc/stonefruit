
use constraints::Constraint;
use types::{TypeLabel, Polytype, Storable, VariableLabel};


/// See documentation for `Ast`.
#[derive(Debug, PartialEq, Eq, Hash, Clone, PartialOrd, Ord)]
pub enum AstBone<C: Constraint, U: Storable, P: Storable> {
    Var(VariableLabel),
    Let(VariableLabel, Box<Ast<C, U, P>>, Box<Ast<C, U, P>>),
    Fun(Box<Ast<C, U, P>>, Box<Ast<C, U, P>>),
    Apply(Box<Ast<C, U, P>>, Box<Ast<C, U, P>>),
    Const(Polytype<C, U, P>),
}

/// Ast and AstBone go together: Ast includes the metadata for each
/// part, and AstBone is the part that changes for each kind of ast node.
#[derive(Debug, PartialEq, Eq, Hash, Clone, PartialOrd, Ord)]
pub struct Ast<C: Constraint, U: Storable, P: Storable> {
    pub position: P,
    pub typelabel: TypeLabel<P>,
    pub bone: AstBone<C, U, P>,
}

impl<C: Constraint, U: Storable, P: Storable> Storable for AstBone<C, U, P> {}

impl<C: Constraint, U: Storable, P: Storable> Ast<C, U, P> {

    pub fn new(position: P, bone: AstBone<C, U, P>) -> Ast<C, U, P> {
        Ast {
            position: position.clone(),
            bone: bone,
            typelabel: TypeLabel::new(position),
        }
    }

    /// Get the variable name if it's a var.
    pub fn variable_name(&self) -> Option<VariableLabel> {
        match &self.bone {
            &AstBone::Var(ref name) => Some(name.to_string()),
            _ => None,
        }
    }

}
