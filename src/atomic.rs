
use std::sync::atomic::{AtomicUsize, Ordering, ATOMIC_USIZE_INIT};

static INDEX: AtomicUsize =  ATOMIC_USIZE_INIT;

pub fn new() -> usize {
    INDEX.fetch_add(1, Ordering::SeqCst)
}