
pub mod ast;
pub mod types;
pub mod constraints;
mod atomic;
mod reconstruction;
mod letenvironment;
mod quantifiable;
mod phi;


use constraints::Constraint;
use types::Storable;
use ast::Ast;
use phi::{Phi, PhiResult};
use letenvironment::LetEnvironment;
use reconstruction::Reconstruct;

pub fn solve<C, U, P>(ast: Ast<C, U, P>) -> PhiResult<Phi<C, U, P>, P>
 where C: Constraint, U: Storable, P: Storable {
    let mut env = LetEnvironment::new();
    let mut phi = Phi::new();
    ast.reconstruct(&mut env, &mut phi)?;
    // phi.unify();
    Ok(phi)
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
