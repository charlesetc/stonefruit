
use phi::Phi;
use types::{Storable, KindingEnvironment, TypeLabel, Monotype, Kind, Polytype};
use constraints::Constraint;
use std::collections::{BTreeSet, BTreeMap};
use letenvironment::LetEnvironment;

/// Basically, it duplicates a type using a maping from previous
/// typelabels to their new ones. (i.e. theta == {a1 -> b1, etc})
pub trait Substitutable<U: Storable, P: Storable> {
    fn substitute(&self, &BTreeMap<TypeLabel<P>, TypeLabel<P>>) -> Self;
}

/// There is a function called `free_variables` that's used within the
/// unification algorithm and the reconstruction algorithm. The idea
/// is that we are able to figure out which variables in the entire
/// polytype are still generic.
pub trait Quantifiable<C: Constraint, U: Storable, P: Storable> : Substitutable<U, P> {
    fn accumulate_variables(&self, phi: &Phi<C, U, P>, k: &KindingEnvironment<C, U, P>, fv: &mut BTreeSet<TypeLabel<P>>);

    fn free_variables(&self, phi: &Phi<C, U, P>, k: &KindingEnvironment<C, U, P>) -> BTreeSet<TypeLabel<P>> {
        let mut variables = BTreeSet::new();
        self.accumulate_variables(phi, k, &mut variables);
        variables
    }
}

impl<U: Storable, P: Storable> Substitutable<U, P> for Monotype<U, P> {
    fn substitute(&self, theta: &BTreeMap<TypeLabel<P>, TypeLabel<P>>) -> Monotype<U, P> {
        use self::Monotype::*;
        match self {
            &Base(ref u) => Base(u.clone()),
            &Generic(ref typelabel) => Generic(theta[typelabel].clone()),
            &Arrow(ref a, ref b) => Arrow(theta[a].clone(), theta[b].clone()),
        }
    }
}

impl<C: Constraint, U: Storable, P: Storable> Quantifiable<C, U, P> for Monotype<U, P> {
    fn accumulate_variables(&self, phi: &Phi<C, U, P>, k: &KindingEnvironment<C, U, P>, fv: &mut BTreeSet<TypeLabel<P>>) {
        use self::Monotype::*;
        match self {
            &Base(_) => (),
            &Generic(ref typelabel) => {
                fv.insert(typelabel.clone());
                k.get(typelabel).map(|kind| kind.accumulate_variables(phi, k, fv));
            },
            &Arrow(ref arg, ref ret) => {
                phi.get(arg).accumulate_variables(phi, k, fv);
                phi.get(ret).accumulate_variables(phi, k, fv);
            },
        };
    }
}

impl<C: Constraint, U: Storable, P: Storable> Substitutable<U, P> for Kind<C, U, P> {
    fn substitute(&self, theta: &BTreeMap<TypeLabel<P>, TypeLabel<P>>) -> Kind<C, U, P> {
        let new_relations = self.relations.iter()
                 .map(|(field, monotype)| (field.clone(), monotype.substitute(theta)))
                 .collect::<BTreeMap<_,_>>();
        Kind {
            constraint: self.constraint.clone(),
            relations: new_relations,
        }
    }
}

impl<C: Constraint, U: Storable, P: Storable> Quantifiable<C, U, P> for Kind<C, U, P> {
    fn accumulate_variables(&self,
                            phi: &Phi<C, U, P>,
                            k: &KindingEnvironment<C, U, P>,
                            fv: &mut BTreeSet<TypeLabel<P>>) {
        for (_, monotype) in self.relations.iter() {
            monotype.accumulate_variables(phi, k, fv);
        }
    }
}


impl<C: Constraint, U: Storable, P: Storable> Substitutable<U, P> for Polytype<C, U, P> {
    fn substitute(&self, theta: &BTreeMap<TypeLabel<P>, TypeLabel<P>>) -> Polytype<C, U, P> {
        Polytype {
            kinding_environment: self.kinding_environment.iter()
                .map(|(typelabel, kind)| {
                    let typelabel = theta[typelabel].clone();
                    let kind = kind.substitute(&theta);
                    (typelabel, kind)
                })
                .collect::<BTreeMap<_,_>>(),
            monotype: self.monotype.substitute(&theta),
        }
    }
}

impl<C: Constraint, U: Storable, P: Storable> Quantifiable<C, U, P> for Polytype<C, U, P> {
    fn accumulate_variables(&self,
                            phi: &Phi<C, U, P>,
                            k: &KindingEnvironment<C, U, P>,
                            fv: &mut BTreeSet<TypeLabel<P>>) {
        // the clone here makes sense:
        // we're making a whole new environment.
        let mut kenv = k.clone();
        kenv.extend(self.kinding_environment.clone());
        self.monotype.accumulate_variables(phi, &kenv, fv);
        // subtract the alpha...alpha quantified by this polytype.
        for label in self.kinding_environment.keys() {
            fv.remove(label);
        }
    }
}


impl<C: Constraint, U: Storable, P: Storable> Substitutable<U, P> for LetEnvironment<C, U, P> {
    fn substitute(&self, theta: &BTreeMap<TypeLabel<P>, TypeLabel<P>>) -> LetEnvironment<C, U, P> {
        LetEnvironment {
            bindings: self.bindings.iter().map(|&(ref label, ref ptype)|
                (label.clone(), ptype.substitute(theta))
            ).collect::<Vec<_>>()
        }
    }
}

impl<C: Constraint, U: Storable, P: Storable> Quantifiable<C, U, P> for LetEnvironment<C, U, P> {
    fn accumulate_variables(&self,
                            phi: &Phi<C, U, P>,
                            k: &KindingEnvironment<C, U, P>,
                            fv: &mut BTreeSet<TypeLabel<P>>) {
        for &(_, ref ptype) in self.bindings.iter() {
            ptype.accumulate_variables(phi, k, fv);
        }
    }
}

pub fn duplicate<C, U, P>(p: &Polytype<C, U, P>, phi: &Phi<C, U, P>) -> Polytype<C, U, P>
where C: Constraint, U: Storable, P: Storable {
    let theta = p.free_variables(phi, &BTreeMap::new()).iter()
        .map(|x| (TypeLabel::new_vec(x.positions.clone()), x.clone()))
        .collect::<BTreeMap<_,_>>();

    p.substitute(&theta)
}
